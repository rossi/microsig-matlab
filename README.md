# MicroSIG
Synthetic image generator (SIG) for defocused/astigmatic particle images.

MicroSIG provides realistic particle images for testing PIV/PTV methods involving defocusing or astigmatic particle images. This includes for instance micro-PIV experiments or 3D PTV methods using defocusing or astigmatism to retrieve the out-of-plane particle position. MicroSIG is based on an approximated model of spherical lens and simple ray tracing.

More detail about the MicroSIG can be found in "Rossi M, *Synthetic image generator for defocusing and astigmatic PIV/PTV*, Meas. Sci. Technol, Submitted".


### How it works:
1. Install MicroSIG: Download the package and copy the content in a local directory (optional: add the local directory to your preference path in Matlab). 
2. Run MicroSIG: From your local directory, run the file `microsig.m` (or type `microsig` in the command line if the directory was added to your preference path).
3. Follow the instructions and select: the *setting file* (`.txt`), one or more *data files* (`.txt`), the *destination folder* for the images.
4. An image in `.tif` format will be created in the *destination folder* for each *data file*.

### Example 1: Poiseuille flow
1. Run microsig.
2. *Setting file*: Select `settings-1.txt` from the folder `\examples` (Alternatively, use `settings-1a.txt` to create images with an astigmatic aberration).
3. *Image files*: Select all files in the folder `\examples\example1_poiseuille_flow`.
4. Select a *destination folder* for your synthetic images.

### Example 2: Rotating spheroids
1. Run MicroSIG.
2. *Setting file*: Select `settings-2.txt` from the folder `\examples`. 
3. *Image files*: Select all files in the folder `\examples\example2_rotating_spheroid`.
4. Select a *destination folder* for your synthetic images.

### Setting files:
Setting files must be `.txt` files in `ASCII`, each row must be a text string with: `parameter_name = value`.
MicroSIG uses 14 parameters:
* `magnification`: Magnification of the simulated lens.
* `numerical_aperture`: Numerical aperture of the simulated lens.
* `focal_length`: This parameter must be chosen empirically from comparison with real images. A typical value is 350. 
* `ri_medium`: Refractive index of the immersion medium of the lens, normally 1.
* `ri_lens`: Refractive index of the immersion medium of the lens, normally 1.5
* `pixel_size`: Size of the side of a square pixel (in microns).
* `pixel_dim_x`: Number of pixels in the x-direction of the sensor.
* `pixel_dim_y`: Number of pixels in the y-direction of the sensor.
* `background_mean`: Constant value of the image background.
* `background_noise`: Amplitude of Gaussian noise added to the images.
* `points_per_pixel`: Number of point sources in a particle, normalized for the particle area. Decrease this parameter for large particles. Typical values are between 10 and 20. 
* `n_rays`: Number of rays for point source. Decrease this value to speed up the particle computation. Typical values are between 100 and 500.
* `gain`: Additional gain to increase or decrease the image intensity.
* `cyl_focal_length`: Additional parameter for astigmatic imaging. This parameter must be chosen empirically from comparison with real images. A typical value is 4000. When it is set to 0 no astigmatism is present. 

For examples/templates see the files `settings-1.txt`, `settings-1a.txt`, `settings-2.txt` in the `\examples` folder.



### Data files:
Data files must be a `.txt` files in `ASCII` format and row arranged  (one  particle,  one  line). MicroSIG allows four different inputs:
* `x`, `y`, `z`, `dp`
* `x`, `y`, `z`, `dp`, `c_int`
* `x`, `y`, `z`, `dp`, `ep`, `alpha`, `beta`
* `x`, `y`, `z`, `dp`, `ep`, `alpha`, `beta`,`c_int`

with:
* `x`, `y`: in-plane coordinates in pixels
* `z`: out-of-plane coordinates in microns
* `dp`: particle diameter in microns
* `c_int`: multiplication factor for the particle image intensity (for not-uniform illumination)
* `ep`: elongation factor for non-spherical particles (vertical axis/horizontal axis)
* `alpha`, `beta`: orientation angles for non-spherical particles

For examples/templates see the files in the folders `\examples\example1_poiseuille_flow` and `\examples\example2_rotating_spheroid`.



