function xp = create_particle(D,Ns,Nr)

R = D/2;

V = spiral_sphere(Ns);
V(1:2,V(1,:)>0) = -V(1:2,V(1,:)>0);
x = R*V(1,:); 
y = R*V(2,:); 
z = R*V(3,:); 

V0 = spiral_sphere(Nr+2);
V0 = V0(:,2:end-1);
u = repmat(x,Nr,1);
v = repmat(y,Nr,1);
s = u*0;
t = u*0;

% phs = random('uniform',-pi,pi,1,length(z));
phs = linspace(-pi*20,pi*20,length(z));
cs = cos(phs);
sn = sin(phs);
for k = 1:Ns     
    V = [cs(k) -sn(k) 0; sn(k) cs(k) 0; 0 0 1]*V0;
    V(1,:) = -abs(V(1,:));
    s(:,k) = V(2,:)./V(1,:);
    t(:,k) = V(3,:)./V(1,:);
    u(:,k) = y(k)-s(:,k)*x(k);
    v(:,k) = z(k)-t(:,k)*x(k);
end
xp = [reshape(u,1,Nr*Ns); reshape(v,1,Nr*Ns);...
    reshape(s,1,Nr*Ns); reshape(t,1,Nr*Ns);];


function V = spiral_sphere(N)

gr = (1+sqrt(5))/2;       % golden ratio
ga = 2*pi*(1-1/gr);       % golden angle

ind_p = 0:(N-1);              % particle (i.e., point sample) index
lat = acos(1-2*ind_p/(N-1));  % latitude is defined so that particle index is proportional to surface area between 0 and lat
lon = ind_p*ga;               % position particles at even intervals along longitude

% Convert from spherical to Cartesian co-ordinates
x = sin(lat).*cos(lon);
y = sin(lat).*sin(lon);
z = cos(lat);
V = [x; y; z];


