function microsig(varargin)
% MICROSIG    Function to generate synthetic particle images 
%
%   MICROSIG starts the procedure for generating synthetic images. The  
%   first step is to load the setting file. The second step is to load one 
%   or more data files with particle position and diameter (and eventually 
%   illumination, elongation and orientation). The third step is to select 
%   the destination folder of the images. Finally the corresponding 
%   synthetic images are generated. 
%
%   For more information see readme.md and examples.

if nargin==4
    name_mic = varargin{1};
    folder_data = varargin{2};
    files_data = varargin{3};
    folder_images = varargin{4};
else
    [filename, pathname] = uigetfile('*.txt','select settings (*.txt)');
    if filename==0, return, end
    name_mic = [pathname,filename];
    [files_data, folder_data] = uigetfile('*.txt','Select source data (*.txt)','multiselect','on');
    if isempty(files_data), return, end
    folder_images = uigetdir('','Select destination folder');
    if folder_images==0, return, end
end

% load microscope settings
file_id = fopen(name_mic);
data_mic = textscan(file_id,'%s');
fclose(file_id);
data_mic = data_mic{1};
if length(data_mic)~=42
    disp('Wrong settings. Check formatting of input file')
    return
else
    for ii = 1:3:40
        eval(['mic.',data_mic{ii},' = ',data_mic{ii+2},';'])
    end
end


if ~iscell(files_data), dum = files_data; files_data = cell(1,1); files_data{1} = dum; end
if ~isempty(folder_images), folder_images(end+1) = '\'; end


n_images = length(files_data);
tic
n_particles = 0;
for ii = 1:n_images
    disp(['creating image ',num2str(ii),' of ',num2str(n_images),' ...'] )
    pos = importdata([folder_data,files_data{ii}]);  
    im_test = take_image(mic,pos);
    n_particles = n_particles+size(pos,1); 
    imwrite(uint16(im_test),[folder_images,files_data{ii}(1:end-3),'tif'])
end
time_tot = toc;
disp(['total time: ',num2str(time_tot),' sec'])
disp(['particles per images: ',num2str(n_particles/n_images)])
disp(['time per particle: ',num2str(time_tot/n_particles),' sec'])



